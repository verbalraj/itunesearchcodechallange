//
//  Helper.swift
//  iTuneSearchDemoApp
//
//  Created by DataCore Inc  on 5/5/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import Foundation

enum InAppNavigationBarTitle {
    //If possible for ease please use class name for the title of nav bar
    static let viewControllerNavBarTitle = "Home"
    static let detailViewControllerNavBarTitle = "Artist Detail"
}

enum InAppStoryBoardName {
    static let detailView = "DetailView"
    static let detailViewIdentifier = "DetailViewController"
}
