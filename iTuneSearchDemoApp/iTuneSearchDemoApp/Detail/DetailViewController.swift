//
//  DetailViewController.swift
//  iTuneSearchDemoApp
//
//  Created by DataCore Inc  on 5/5/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import UIKit
import SearchPackage

final class DetailViewController: UIViewController {

    @IBOutlet weak var artWorkImageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var genere: UILabel!
    @IBOutlet weak var iTuneURL: UIButton!
    var iTuneURLString = ""
    var result: SearchResultJsonKeys?
    var url: URL?
    
    
   final fileprivate func setUpDetailView(){
        self.title = InAppNavigationBarTitle.detailViewControllerNavBarTitle
        DispatchQueue.main.async {
            let newURL = self.result?.artwork?.replacingOccurrences(of: "100", with: "300") // onlu to increase the quality of image i increase the size of image, Considering they are having very large image and giving in response what ever i will pass the size
            // https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/24/46/97/24469731-f56f-29f6-67bd-53438f59ebcb/source/350x350bb.jpg
            guard let urlString = newURL, let name = self.result?.name, let genere = self.result?.genre, let url = self.result?.url,
                let imageUrl = URL(string: urlString),
                let imageData = try? Data(contentsOf: imageUrl) else {
                    return
            }
            DispatchQueue.main.async { [weak self] in
                self?.artWorkImageView.image = UIImage(data: imageData)
                self?.name.text = name
                self?.genere.text = genere
                self?.iTuneURLString = url
            }
        }
    }
    
    @IBAction final func openURLAction() {
        if let url = URL(string: iTuneURLString) {
            UIApplication.shared.open(url)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDetailView()
    }
    

}
