//
//  SearchResultTableViewCell.swift
//  iTuneSearchDemoApp
//
//  Created by DataCore Inc  on 5/5/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import UIKit
import SearchPackage
class SearchResultTableViewCell: UITableViewCell {
    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(_ result: SearchResultJsonKeys) {
        DispatchQueue.main.async {
            self.label1.text = result.name // label1 is for title
            self.label2.text = result.genre // for gener
            self.label3.text = result.country
            DispatchQueue.global().async {
                guard
                    let urlString = result.artwork,
                    let imageUrl = URL(string: urlString),
                    let imageData = try? Data(contentsOf: imageUrl) else {
                        return
                }
                DispatchQueue.main.async { [weak self] in
                    self?.artworkImageView.image = UIImage(data: imageData)
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        artworkImageView.image = nil
        label1.text = nil
        label2.text = nil
        label3.text = nil
    }

    
    class func reUseID() -> String {
        return String(describing: self)
    }
    

}
