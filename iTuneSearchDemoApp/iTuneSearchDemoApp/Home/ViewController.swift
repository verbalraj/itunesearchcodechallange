//
//  ViewController.swift
//  iTuneSearchDemoApp
//
//  Created by DataCore Inc  on 5/4/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import UIKit
import SearchPackage
final class ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var searchResultTableView: UITableView!
    @IBOutlet weak var searchedBar: UISearchBar!
    @IBOutlet weak var noDataFoundLabel: UILabel!
    
    var searchResult = [iTunesResultType : [SearchResultJsonKeys]]()
    var searchTimer = Timer()
    var searchScopes: [iTunesSearchRequest.SearchMedia] = [
        iTunesSearchRequest.SearchMedia.all,
        iTunesSearchRequest.SearchMedia.music,
        iTunesSearchRequest.SearchMedia.movie,
        iTunesSearchRequest.SearchMedia.tvShow,
        iTunesSearchRequest.SearchMedia.podcast,
    ]
    var selectedScopeIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        setupDelegate()
    }
    
    final func toHideTableView(to show: Bool) {
        searchResultTableView.isHidden = show
    }
    
    private final func setupDelegate(){
        searchResultTableView.delegate = self
        searchResultTableView.dataSource = self
        searchedBar.delegate = self
        self.title = InAppNavigationBarTitle.viewControllerNavBarTitle
        toHideTableView(to: true)
    }
    
    final func registerCell(){
        self.searchResultTableView.register(UINib(nibName: SearchResultTableViewCell.reUseID(), bundle: nil), forCellReuseIdentifier: SearchResultTableViewCell.reUseID())
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return searchResult.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionKey = searchResult.keys.compactMap({ $0 })[section]
        guard let resultsForSection = searchResult[sectionKey] else {
            return 0
        }
        return resultsForSection.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return searchResult.keys.compactMap({ $0 })[section].displayTitle
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableViewCell.reUseID(), for: indexPath) as? SearchResultTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = (indexPath.row % 2 == 0) ? UIColor.green.withAlphaComponent(0.05) : UIColor.white
        let sectionKey = searchResult.keys.compactMap({ $0 })[indexPath.section]
        guard let resultsForSection = searchResult[sectionKey] else {
            return UITableViewCell()
        }
        
        let result = resultsForSection[indexPath.row]
        cell.configure(result)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailVC = UIStoryboard(name: InAppStoryBoardName.detailView, bundle: nil).instantiateViewController(identifier:InAppStoryBoardName.detailViewIdentifier) as? DetailViewController {
            let sectionKey = searchResult.keys.compactMap({ $0 })[indexPath.section]
            guard let resultsForSection = searchResult[sectionKey] else {
                return
            }
            
            detailVC.result = resultsForSection[indexPath.row]
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
        
    }
    
    final fileprivate func decodeData(_ data: (Data)) {
        SearchPackage.RequestManager.shared.searchJsonResponse(from: data) { [weak self] (result) in
            switch result {
            case .success(let searchResults):
                DispatchQueue.main.async {
                    self?.handleSearchResults(searchResults)
                }
            case .failure:
                print("Error in decoding data")
                break
            }
        }
    }
    
    final  fileprivate func handleSearchResults(_ results: ([iTunesResultType : [SearchResultJsonKeys]])) {
        if results.count >= 0 {
            
        }
        self.searchResult = results
        toHideTableView(to: false)
        reloadTableView()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTimer.invalidate()
        searchTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(0.30), repeats: false, block: { _ in
            DispatchQueue.main.async {
                self.performSearch(for: searchText)
            }
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchResult.removeAll()
        searchBar.endEditing(true)
        reloadTableView()
        toHideTableView(to: true)
    }
    
    func performSearch(for searchText: String) {
        guard !searchText.isEmpty else {
            searchedBar.endEditing(true)
            reloadTableView()
            return
        }
        guard let searchRequest = iTunesSearchRequest(term: searchedBar.text ?? "", media: searchScopes[selectedScopeIndex]) else {
            return
        }
        
        SearchPackage.RequestManager.shared.searchKeyword(with: searchRequest) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                self.decodeData(data)
            case .failure:
                print("Use debug print here to display failure error")
                break
            }
        }       }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.searchResultTableView.reloadData()
        }
    }
    
}

