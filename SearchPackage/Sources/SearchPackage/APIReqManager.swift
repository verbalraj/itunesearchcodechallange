//
//  File.swift
//  
//
//  Created by DataCore Inc  on 5/4/20.
//

import Foundation

public class RequestManager {
    public static var shared = RequestManager()
    //Blank init
    private init() {
    }
    
    public func searchKeyword(with Request: iTunesSearchRequest, completion: @escaping (Result<Data, APIError>) -> Void) {
        guard let urlRequest = createUrlRequest(Request) else {
            completion(.failure(.malformedUrlRequestError))
            return
        }
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard error == nil,
                (response as? HTTPURLResponse)?.statusCode == 200,
                let data = data else {
                    completion(.failure(.generalError))
                    return
            }
            guard let json: [String: AnyHashable] = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyHashable],
                let response = self.createDataObject(from: json) else {
                    completion(.failure(.decodingError))
                    print("Error occured")
                    return
            }
            completion(.success(response))
        }.resume()
    }
    
    public func searchJsonResponse(from jsonData: Data,
                                   completion: @escaping (Result<[iTunesResultType: [SearchResultJsonKeys]], APIError>) -> Void) {
        guard let response = try? JSONDecoder().decode([String: [SearchResultJsonKeys]].self, from: jsonData) else {
            completion(.failure(.decodingError))
            return
        }
        var responseArr = [iTunesResultType: [SearchResultJsonKeys]]()
        
        for resp in response {
            if let type = iTunesResultType(rawValue: resp.key.lowercased()) {
                responseArr[type] = resp.value
            }
        }
        
        completion(.success(responseArr))
    }
}

// MARK: - Private Helpers
extension RequestManager {
    func createUrlRequest(_ searchRequest: iTunesSearchRequest) -> URLRequest? {
        let urlStr = "https://itunes.apple.com/search?" + searchRequest.parameterKeyValue
        guard let url = URL(string: urlStr) else {
            return nil
        }
        
        return URLRequest(url: url,
                          cachePolicy: .returnCacheDataElseLoad,
                          timeoutInterval: 20)
    }
    
    func createDataObject(from json: [String: AnyHashable]) -> Data? {
        guard let results = json["results"] as? [AnyHashable] else {
            return nil
        }
        var response = [String: [AnyHashable]]()
        results.forEach({ result in
            if let media = result as? [String: AnyHashable],
                let kind = media["kind"] as? String {
                if var existingValues = response[kind] {
                    existingValues.append(result)
                    response.updateValue(existingValues, forKey: kind)
                } else {
                    response[kind] = [result]
                }
            }
        })
        
        let jsonData: Data?
        if #available(iOS 11.0, *) {
            jsonData = try? JSONSerialization.data(withJSONObject: response, options: .sortedKeys)
        } else {
            jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
        }
        return jsonData
    }
}

public enum APIError: Error {
    case malformedUrlRequestError
    case decodingError
    case generalError
}
