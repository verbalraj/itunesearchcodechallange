//
//  File.swift
//  
//
//  Created by DataCore Inc  on 5/4/20.
//

import Foundation

/*
"wrapperType": "track",
"kind": "song",
"artistId": 909253,
"collectionId": 1440857781,
"trackId": 1440857786,
"artistName": "Jack Johnson",
"collectionName": "In Between Dreams (Bonus Track Version)",
"trackName": "Better Together",
"collectionCensoredName": "In Between Dreams (Bonus Track Version)",
"trackCensoredName": "Better Together",
"artistViewUrl": "https://music.apple.com/us/artist/jack-johnson/909253?uo=4",
"collectionViewUrl": "https://music.apple.com/us/album/better-together/1440857781?i=1440857786&uo=4",
"trackViewUrl": "https://music.apple.com/us/album/better-together/1440857781?i=1440857786&uo=4",
"previewUrl": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview118/v4/94/25/9c/94259c23-84ee-129d-709c-577186cbe211/mzaf_5653537699505456197.plus.aac.p.m4a",
"artworkUrl30": "https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/24/46/97/24469731-f56f-29f6-67bd-53438f59ebcb/source/30x30bb.jpg",
"artworkUrl60": "https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/24/46/97/24469731-f56f-29f6-67bd-53438f59ebcb/source/60x60bb.jpg",
"artworkUrl100": "https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/24/46/97/24469731-f56f-29f6-67bd-53438f59ebcb/source/100x100bb.jpg",
"collectionPrice": 11.99,
"trackPrice": 1.29,
"releaseDate": "2005-03-01T12:00:00Z",
"collectionExplicitness": "notExplicit",
"trackExplicitness": "notExplicit",
"discCount": 1,
"discNumber": 1,
"trackCount": 15,
"trackNumber": 1,
"trackTimeMillis": 207679,
"country": "USA",
"currency": "USD",
"primaryGenreName": "Rock",
"isStreamable": true

*/

public struct SearchResultJsonKeys: Codable {
    public private(set) var id: Int
    public private(set) var name: String?
    public private(set) var artwork: String?
    public private(set) var genre: String?
    public private(set) var url: String?
    public private(set) var kind: String // for to sort as per result type in table view which display the search result to user.
    public private(set) var previewUrl: String?
    public private(set) var country: String?
    public var type: iTunesResultType {
        return iTunesResultType(rawValue: kind) ?? .unKnown
    }

    enum CodingKeys: String, CodingKey {
        case id = "trackId" // as in requirement
        case name = "trackName"
        case artwork = "artworkUrl100"
        case genre = "primaryGenreName"
        case url = "trackViewUrl"
        case kind
        case previewUrl
        case country
    }
}

public enum iTunesResultType: String, RawRepresentable, Codable {
    case album
    case book
    case coachedAudio = "coached-audio"
    case featureMovie = "feature-movie"
    case interactiveBooklet = "interactive-booklet"
    case musicVideo = "music-video"
    case pdfPodcast = "pdf podcast"
    case podcastEpisode = "podcast-episode"
    case softwarePackage = "software-package"
    case song
    case tvEpisode = "tv-episode"
    case artist
    case unKnown

    public var displayTitle: String {
        switch self {
        case .book:
            return "Books"
            
        case .album:
            return "Albums"
            
        case .coachedAudio:
            return "Coached Audio"
            
        case .featureMovie:
            return "Movies"
            
        case .interactiveBooklet:
            return "Interactive Booklet"
            
        case .musicVideo:
            return "Music Video"
            
        case .pdfPodcast:
            return "PDF Podcast"
            
        case .podcastEpisode:
            return "Podcast Episodes"
            
        case .softwarePackage:
            return "Software"
            
        case .song:
            return "Songs"
            
        case .tvEpisode:
            return "TV Episodes"
            
        case .artist:
            return "Artists"
            
        case .unKnown:
            return "unKnown"
        }
    }
}

