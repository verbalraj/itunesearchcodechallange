import XCTest

import SearchPackageTests

var tests = [XCTestCaseEntry]()
tests += SearchPackageTests.allTests()
XCTMain(tests)
